[![作者](https://img.shields.io/badge/%E4%BD%9C%E8%80%85-%E5%B0%8F%E5%B8%85%E4%B8%B6-7AD6FD.svg)](https://www.ydxiaoshuai.cn/)

### 💳感谢支持


| 三叔 | https://gitee.com/zoy1985 |
|----|--------------------------|

### 感谢贡献代码


| 周维 | https://gitee.com/bejson |[GIF压缩方法](https://gitee.com/xshuai/imagetool/blob/master/src/main/java/cn/xsshome/imagetool/util/GifImageUtil.java)|
|----|--------------------------|--------------------------|



### 关注码小帅获取最新功能更新

![码小帅](https://images.gitee.com/uploads/images/2021/0518/164321_b70506da_131538.jpeg "码小帅.jpg")

```
       ├── cn.xsshome.imagetool            //包名
       ├── convert                               
       │       └── ImageToChar             //图片转字符图片、文本方法  
       ├── slideverifycode        
       │       └── SlideVerifyCodeGenerateUtil      //滑块验证码工具类代码       
       │       └── SlideVerifyCode         //滑块验证码对象   
       ├── util        
       │       └── AnimatedGifEncoder      //GIF所需工具类代码
       │       └── Base64Util              //图片数据转base64编码工具类     
       │       └── GifDecoder              //Gif图片处理工具类代码
       │       └── GifImageUtil            //GIF图片添加文字特效工具类代码  
       │       └── GongGeUtil              //九宫格图片处理工具类代码      
       │       └── LZWEncoder              //图片处理所需工具类代码
       │       └── MergeImageUtil          //图片特效合并工具类代码
       │       └── NeuQuant                //图片处理所需工具类代码
       │       └── PngColoringUtil         //透明图片增加背景色工具类代码
       │       └── RotateImageUtil         //图片旋转工具类代码
       │       └── ImageLosslessUtil       //图片无损保存工具类代码   
       │       └── MergeImageFontUtil      //图片文字合并工具类代码   
       │       └── ImageGrayUtil           //图片灰度处理工具类代码   
       │       └── ImageDPIHandleUtil      //修改图片DPI工具类代码(仅支持JPG)-基于com.sun.image.codec
       │       └── ImagePixelUtil          //像素风工具类代码
       │       └── ImageWaterMarkUtil      //图片增加水印工具类代码
       │       └── DoubleExposureUtil      //图片双重曝光工具类代码
       ├── zoom   
       └──     └── ImageHelper             //图片缩放工具类代码 
```

### 示例代码
```
public class Sample {
	public static void main(String[] args) throws Exception {
		ImageToChar.load("G:/phone.jpg", "F:/gif/woman.txt");//静态图片转字符保存为txt文件
		ImageToChar.loadGif("C:/Users/Administrator/Desktop/页面录屏显示.gif", "F:/gif/");//动图转为动态的字符图片
		BufferedImage bi = null;
		bi = ImageIO.read(new File("G:/body.jpg"));
        String bytePic = ImageToChar.txtToImageByBase64(bi);//静态图转字符 返回转换后图片的base64
        System.out.println(bytePic);
	}
}
```
### 透明图片增加背景色示例代码

```
    public static void main(String[] args) throws Exception{
        String image = "F:\\testimg\\1011.png";//原始图片路径
        String resultImage = "F:\\testimg\\10111700.jpg";//处理后保存的图片路径
        changePNGBackgroudColor(image,resultImage, Color.pink);//Color.pink 即图片背景颜色
    }
```

### 图片旋转示例代码
```
public class RotateSample {
    public static void main(String[] args) throws  Exception {
        long start = System.currentTimeMillis();
        BufferedImage src = ImageIO.read(new File("E:\\testimg\\glassess.png"));//原图片本地路径
        BufferedImage des = RotateImageUtil.rotateImage(src,20);//旋转的角度
        ImageIO.write(des, "png", new File("E:\\testimg\\glassess2.png"));//旋转后保存的图片
        long end = System.currentTimeMillis();
        System.out.println("开始时间:" + start+ "; 结束时间:" + end+ "; 总共用时:" + (end - start) + "(ms)");
    }
}
```
### GIF图片加文字特效示例代码
```
    public static void main(String[] args) throws Exception {
        GifImageUtil gifImageUtil = new GifImageUtil();
        String imagesavePath  = "C:\\Users\\xiaoshuai\\Desktop";//图片保存路径
        String imagesaveName = String.valueOf(System.currentTimeMillis());//图片保存名称 不包含后缀名
        String image  = "C:\\Users\\xiaoshuai\\Desktop\\db.gif";//原始图片
        String result = gifImageUtil.gifAddText(imagesavePath,imagesaveName,"微软雅黑",25,Color.pink,image,"图片添加文字","测试一下","1234","4567");
        System.out.println(result);
    }
```

### 白底图片转换透明底图片示例代码
```
    public static void main(String[] args) {
        System.out.println(PngConvertUtil.transparentImg("F:/testimg/hand.jpg", "F:/testimg/hand2020.png"));
    }
```

### 增加水印示例代码
```
    public static void main(String[] args) {
        //水印图片在指定位置
        BufferedImage image1 =  ImageWaterMarkUtil.addImgWaterMark("F://testimg//water.png", "F://testimg//gjy2.jpg", 4);
        File file = new File("F:\\testimg\\2020082701.jpg");
        ImageIO.write(image1, "jpg", file);
        //文本水印 铺满图片
        BufferedImage image2 = ImageWaterMarkUtil.addFullTextWaterMark("F://testimg//gjy2.jpg","小帅丶代码");
        File file2 = new File("F:\\testimg\\2020082702.jpg");
        ImageIO.write(image2, "jpg", file2);
        //图片水印 铺满图片
        BufferedImage image3 = ImageWaterMarkUtil.addFullImgWaterMark("F://testimg//gjy2.jpg","F://testimg//water.png");
        File file3 = new File("F:\\testimg\\2020082703.jpg");
        ImageIO.write(image3, "jpg", file3);
    }
```

### GIF图片加文字特效示例图
#### 原始图
![原始图](https://images.gitee.com/uploads/images/2019/0516/174359_5b52fe9a_131538.gif "原始图.gif")
#### 转换图
![转换图](https://images.gitee.com/uploads/images/2019/0516/174432_2af80fd2_131538.gif "转换图.gif")

### 给图片指定区域增加矩形框
#### 原始图
![原始图](https://images.gitee.com/uploads/images/2019/0815/182016_0b2cc009_131538.jpeg "原始图")
#### 转换图
![转换图](https://images.gitee.com/uploads/images/2019/0815/182040_bb819a35_131538.jpeg "转换图")

### 给透明图片增加背景图
```
    public static void main(String[] args) throws Exception{
        //最好保证宽或高一致  
        BufferedImage src = ImageIO.read(new File("F:\\testimg\\gjy2.jpg"));//背景图
        BufferedImage png = ImageIO.read(new File("F:\\testimg\\banner.png"));//透明图
        BufferedImage image = MergeImageUtil.mergePendant(src, png, 0, 0, 1);
        File file = new File("F:\\testimg\\banner320.jpg");//输出图片路径
        ImageIO.write(image, "jpg", file);
    }
```
#### 原始图
##### 透明图
![透明图](https://images.gitee.com/uploads/images/2020/0320/160106_fc19f62c_131538.png "banner.png")
##### 背景图
![背景图](https://images.gitee.com/uploads/images/2020/0320/160135_d92b311e_131538.jpeg "gjy2.jpg")
#### 效果图
![效果图](https://images.gitee.com/uploads/images/2020/0320/160211_4895c067_131538.jpeg "banner320.jpg")

### 给图片增加水印
#### 原图
![原图](https://images.gitee.com/uploads/images/2020/0827/172243_a7975f28_131538.jpeg "原图")
#### 文本内容

```
小帅丶代码
```
#### 水印图片 点击空白即可看到png图片中的内容
![水印图片](https://images.gitee.com/uploads/images/2020/0827/172443_45847ea1_131538.png "水印图片")
#### 增加文本水印铺满图片
![增加文本水印铺满图片](https://images.gitee.com/uploads/images/2020/0827/172320_22e1ee88_131538.jpeg "增加文本水印铺满图片")
#### 增加图片水印铺满图片
![增加图片水印铺满图片](https://images.gitee.com/uploads/images/2020/0827/172345_0fd36ffd_131538.jpeg "增加图片水印铺满图片")
#### 增加图片水印指定位置
![增加图片水印指定位置](https://images.gitee.com/uploads/images/2020/0827/172409_255894e7_131538.jpeg "增加图片水印指定位置")

### 给图片增加文字(消除锯齿、无损保存处理后图)
```
public class FontImgMergeSample {
    /** 字体 */
    private static String FONT_FAMILY = "方正粗黑宋简体";
    /** 字体大小 */
    private static Integer FONT_SIZE = 88;
    /** 颜色 */
    private static Color FONT_COLOR = new Color(255,255,255);

    public static void main(String[] args) throws Exception{
        long startTime = System.currentTimeMillis();
        //原图本地路径、保存图的本地路径
        String sourcePath = "F:\\testfile\\testimg\\banner.jpg";
        String targetPath = "F:\\testfile\\testimg\\mergebanner.jpg";
        BufferedImage image = ImageIO.read(new File(sourcePath));
        //文本
        String content = "小帅测试代码";
        Graphics2D g = (Graphics2D) image.getGraphics();
        //填充文字 从左往右
        MergeImageFontUtil.setContentToImgLR(content, g, 100, 200,FONT_FAMILY,FONT_SIZE,FONT_COLOR);
        ImageLosslessUtil.saveLosslessImage(image, targetPath, ImageType.IMAGE_TYPE_JPG);
        System.out.println("总耗时"+(System.currentTimeMillis()-startTime));
    }
}
```
#### 原图
![原图](src/main/files/banner.jpg)

#### 处理后的图片
![处理后的图片](src/main/files/mergebanner.jpg)

### 图片灰度处理
```
public static void main(String[] args) throws ImageTypeException, Exception {
    String sourcePath = "原图本地路径";
    String targetPath = "新图保存的本地路径";
    String imageSuffix = "jpeg";//图片保存格式
    //图片灰度处理 推荐
    BufferedImage bufferedImage = ImageGrayUtil.grayImage3ByteBGR(sourcePath);
    //图片灰度处理 纯黑
    //BufferedImage bufferedImage = ImageGrayUtil.grayImageByteGRAY(sourcePath);
    //无压缩保存图片
    ImageLosslessUtil.saveLosslessImage(bufferedImage,targetPath,imageSuffix);
}
```

#### 原图
![原图](src/main/files/gray_source.jpeg)

#### 处理后的图片 TYPE_BYTE_GRAY
![处理后的图片](src/main/files/gray_gray.jpeg)

#### 处理后的图片 TYPE_3BYTE_BGR 
![处理后的图片](src/main/files/gray_3byte.jpeg)


### 百叶图
![处理后的图片](src/main/files/aa01.jpg)               ![处理后的图片](src/main/files/aa02.jpg)


### 九宫格图
![处理后的图片](src/main/files/one.png)   


### 像素风原图
![原图图片](src/main/files/source_pixel.jpg)   

### 像素风处理后图 
![处理后的图片](src/main/files/pixel.jpg)  
