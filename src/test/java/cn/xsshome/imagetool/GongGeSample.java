package cn.xsshome.imagetool;

import cn.xsshome.imagetool.util.GongGeUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

/**
 * Description 宫格图
 * ProjectName imagetool
 * Created by 小帅丶 on 2022-06-17
 * Version 1.0
 */

public class GongGeSample {

    public static void main(String[] args) throws Exception {

        List<BufferedImage> bufferedImages = GongGeUtil.generateNineGongGe("F:\\testfile\\testimg\\gray_source.jpeg");

        for (int i = 0; i < bufferedImages.size(); i++) {
            File file = new File("F:\\testfile\\testimg\\gongge\\"+(i+1)+".jpg");
            Thread.sleep(100);
            ImageIO.write(bufferedImages.get(i), "jpg", file);
        }
    }
}
