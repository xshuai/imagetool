package cn.xsshome.imagetool.constants;

/**
 * Description 图片类型
 * ProjectName imagetool
 * Created by 小帅丶 on 2022-04-18 13:53.
 * Version 1.0
 */

public class ImageType {

    public static String IMAGE_TYPE_PNG = "png";

    public static String IMAGE_TYPE_JPG = "jpg";

    public static String IMAGE_TYPE_WBPP = "webpp";

    public static String IMAGE_TYPE_BMP = "bmp";

    public static String IMAGE_TYPE_GIF = "gif";
}
