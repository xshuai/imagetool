package cn.xsshome.imagetool.util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author 小帅丶
 * @className MirrorImageUtil
 * @Description 镜像图片
 * @Date 2021-12-15-11:02
 **/
public class MirrorImageUtil {
    /**
     * @Author 小帅丶
     * @Description 转图片镜像
     * @Date  2021-12-15 11:11
     * @param fileByte - 图片byte
     * @return java.awt.image.BufferedImage
     **/
    public static BufferedImage getMirrorImage(byte[] fileByte) throws Exception{
        BufferedImage image = ImageIO.read(byte2Input(fileByte));
        return dealMirrorImage(image);
    }

    /**
     * @Author 小帅丶
     * @Description 转图片镜像
     * @Date  2021-12-15 11:11
     * @param filePath - 图片路径
     * @return java.awt.image.BufferedImage
     **/
    public static BufferedImage getMirrorImage(String filePath) {
        File file = null;
        BufferedImage image = null;
        try {
            file = new File(filePath);
            image = ImageIO.read(file);
            image = dealMirrorImage(image);
            return image;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
    /**
     * @Author 小帅丶
     * @Description 转图片镜像
     * @Date  2021-12-15 11:11
     * @param image - BufferedImage对象
     * @return java.awt.image.BufferedImage
     **/
    private static BufferedImage dealMirrorImage(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        for (int j = 0; j < height; j++) {
            int l = 0, r = width - 1;
            while (l < r) {
                int pl = image.getRGB(l, j);
                int pr = image.getRGB(r, j);
                image.setRGB(l, j, pr);
                image.setRGB(r, j, pl);
                l++;
                r--;
            }
        }
        return image;
    }
    /**
     * @Author 小帅丶
     * @Description byte2Input
     * @Date  2021-12-15 11:19
     * @param buf - buf数据
     * @return java.io.InputStream
     **/
    public static final InputStream byte2Input(byte[] buf) {
        return new ByteArrayInputStream(buf);
    }
}
