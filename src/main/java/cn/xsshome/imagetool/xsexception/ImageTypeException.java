package cn.xsshome.imagetool.xsexception;

/**
 * Description 自定义异常
 * ProjectName imagetool
 * Created by 小帅丶 on 2022-05-13 11:04.
 * Version 1.0
 */

public class ImageTypeException extends Throwable {
    private String zh_msg;
    public ImageTypeException() {
    }

    public ImageTypeException(String message,String zh_msg) {
        super(message);
        this.zh_msg = zh_msg;
    }

    public String getZh_msg() {
        return zh_msg;
    }

    public void setZh_msg(String zh_msg) {
        this.zh_msg = zh_msg;
    }
}
